class Path {
  constructor (zOffset, material, geometryOptions) {
    this.zOffset = zOffset;
    this.material = material;
    this.geometryOptions = geometryOptions;
    this.segments = [];
  }

  origin () {
    return this.segments[0];
  }

  end () {
    return this.segments[this.segments.length - 1];
  }

  empty () {
    return this.segments.length == 0;
  }

  addSegment (knots) {
    const points = [];

    points.push(new THREE.Vector3());
    points.push(new THREE.Vector3(0, 0, this.zOffset));
    for (let i = 0; i < knots.length; i++) {
      let knot = knots[i];
      let z = (i + 2) * this.zOffset;

      points.push(new THREE.Vector3(knot.x, knot.y, z));
    }
    points.push(new THREE.Vector3(0, 0, points.length * this.zOffset));
    points.push(new THREE.Vector3(0, 0, points.length * this.zOffset));

    const segment = new PathSegment(points);
    this.segments.push(segment);

    return segment;
  }

  shiftSegment () {
    return this.segments.shift();
  }

  getAlignedMeshes () {
    const meshes = this.getMeshes();

    for (let i = 1; i < this.segments.length; i++) {
      let lastPoint = this.segments[i - 1].end();
      meshes[i].position.copy(lastPoint);
    }

    return meshes;
  }

  getMeshes () {
    return this.segments.map((segment) => {
      return segment.getMesh(this.material, this.geometryOptions);
    });
  }

  getGeometries () {
    return this.segments.map((segment) => {
      return segment.getGeometry(this.geometryOptions);
    });
  }
}
