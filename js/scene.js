class Scene {
  constructor (options = {}) {
    this.options = options;
    this.root = new THREE.Scene();
    this.root.name = 'scene';
    this.assetsManager = new AssetsManager();

    this.assetsManager.onLoad = (assets) => {
      this.assets = assets;
      this.build();
      this.onLoad();
    };

    this.assetsManager.onProgress = (progress) => {
      this.onProgress(progress);
    };
  }

  load () {
    this.loadAssets();
  }

  loadAssets () {
    const bgUrls = [
      'universe/px.png',
      'universe/nx.png',
      'universe/py.png',
      'universe/ny.png',
      'universe/pz.png',
      'universe/nz.png'
    ];

    this.assetsManager.loadCubeMap(bgUrls, 'background');

    this.assetsManager.loadModels({ url: 'robot/scene.gltf', name: 'robot' });
  }

  build () {
    this.root.background = this.assets.cubemaps.background;

    this.addLights();

    this.addPath();

    this.addRobot();

    this.addTargets();
  }

  addLights () {
    const dirLight = new THREE.DirectionalLight(0xffffff, 1.5);
    dirLight.name = 'dirLight';
    dirLight.position.set(0, 0, this.options.camera.far);
    dirLight.target.position.set(0, 0, 0);
    this.root.add(dirLight);

    const ambientLight = new THREE.AmbientLight(0xffffff, 1);
    ambientLight.name = 'ambientLight';
    this.root.add(ambientLight);
  }

  addPath () {
    const { num_knots, range, radius, t_segs, r_segs } = this.options.path;

    const material = new THREE.MeshLambertMaterial({
      color: 0xffffff,
      envMap: this.root.background
    });

    const geometryOptions = {
      radius: radius,
      t_segs: t_segs,
      r_segs: r_segs
    };

    const path = new Path(-range.z, material, geometryOptions);

    for (let n = 0; n < 2; n++) {
      let knots = [];
      for (let i = 0; i < num_knots - 4; i++) {
        let x = range.x * (0.5 - Math.random());
        let y = range.y * (0.5 - Math.random());

        knots.push(new THREE.Vector2(x, y));
      }
      path.addSegment(knots);
    }

    const pathRoot = new THREE.Object3D();
    pathRoot.name = 'pathRoot';
    pathRoot.userData.path = path;
    pathRoot.add(...path.getAlignedMeshes());


    this.root.add(pathRoot);
  }

  addRobot () {
    const robotRoot = new THREE.Object3D();
    robotRoot.name = 'robotRoot';
    robotRoot.userData.radius = this.options.path.radius + 2;

    const path = this.root.getObjectByName('pathRoot').userData.path;
    robotRoot.lookAt(path.origin().curve.getTangentAt(0));

    const robot = this.assets.models.robot;
    robot.scale.set(0.1, 0.1, 0.1);
    robot.position.set(0, robotRoot.userData.radius, 0);

    robotRoot.add(robot);
    this.root.add(robotRoot);
  }

  addTargets () {
    const pathRoot = this.root.getObjectByName('pathRoot');
    const path = pathRoot.userData.path;

    const { n, scale, color } = this.options.targets;
    const r = this.options.path.radius + 2 * scale;

    let geometry = new THREE.BoxBufferGeometry(scale, scale, scale);

    for (let i = 0; i < path.segments.length; i++) {
      let segment = path.segments[i];
      let mesh = pathRoot.children[i];
      let targets = [];

      let dp = 1 / n;
      let p = 0;

      for (let j = 0; j < n; j++) {
        p = Math.min(1, p + dp);

        let tangent = segment.curve.getTangentAt(p)
        let point = segment.curve.getPointAt(p);

        let material = new THREE.MeshLambertMaterial({ color: color });

        let target = new THREE.Mesh(geometry, material);
        target.userData.initPosition = point;
        target.position.set(point.x, point.y, point.z);
        target.lookAt(tangent);

        targets.push(target);
      }

      let targetsRoot = new THREE.Object3D();
      targetsRoot.name = 'targetsRoot';
      targetsRoot.position.y += r;
      targetsRoot.add(...targets);

      mesh.add(targetsRoot);
    }
  }
}
