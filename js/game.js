class Game {
  constructor (options = {}) {
    this.options = options;

    this.inputManager = new InputManager();

    this.reset();
  }

  reset () {
    this.state = {
      path: {
        motion: {
          p: 0,
          speed: this.options.path.motion.speed || 1
        }
      },

      robot: {
        motion: {
          up: 0,
          rot: 0,
          speed: this.options.robot.motion.speed || { up: 1, rot: 1 },
          limit: this.options.robot.motion.limit || { up: 1, rot: 1 }
        }
      },

      targets: {
        motion: {
          limit: this.options.targets.motion.limit || { x: 0, y: 0, z: 0 }
        }
      }
    }

    this.inputManager.reset();
  }
}
