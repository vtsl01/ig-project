'use-strict';

window.onload = function() {
  const options = {
    ui: {
      playing: false
    },

    renderer: {
      canvas: document.getElementById('canvas'),
      antialias: true
    },

    scene: {
      camera: {
        fov: 90,
        near: 0.1,
        far: 10000,
        position: { x: 0, y: 50, z: -100 }
      },

      path: {
        num_knots: 20,
        range: { x: 100, y: 100, z: 200 },
        radius: 10,
        r_segs: 128,
        t_segs: 128
      },

      targets: {
        n: 64,
        scale: 2,
        color: '#ffffff'
      },
    },

    game: {
      robot: {
        motion: {
          speed: { up: 0.25, rot: 3 },
          limit: { up: 10, rot: 30 }
        }
      },

      path: {
        motion: {
          speed: 10
        }
      },

      targets: {
        motion: {
          limit: { x: 50, y: 3, z: 0 },
        }
      }
    },

    audio : {
      song: 'Rocket',
      fftSize: 128
    },

    animator: {
      timeScale: 10
    }
  };

  // the camera aspect ratio, as well as the renderer size, will be set
  // automatically by the app, depending on the canvas width and height
  const renderer = new THREE.WebGLRenderer(options.renderer);

  const { fov, near, far, position } = options.scene.camera;
  const camera = new THREE.PerspectiveCamera(fov, 1, near, far);
  camera.name = 'camera';
  camera.position.x = position.x;
  camera.position.y = position.y;
  camera.position.z = position.z;

  const scene = new Scene(options.scene);
  const game = new Game(options.game);
  const app = new App(renderer, scene, camera, game);

  const controller = new Controller(app);
  controller.initUI(options.ui);
  controller.showProgressBar();

  scene.onProgress = (progress) => {
    controller.updateProgressBar(progress);
  };

  scene.onLoad = () => {
    scene.root.getObjectByName('robot').add(camera);

    app.animator = new Animator(scene, game, options.animator);

    Utils.dumpObject(scene.root).forEach((line) => { console.log(line); });
    console.log(scene);
    console.log('Starting app');

    controller.hideProgressBar();

    options.ui.playing ? app.start(): app.render();
  };

  const { song, fftSize } = options.audio;
  app.audioManager.onProgress = (progress) => {
    controller.updateProgressBar(progress);
  };

  app.audioManager.onLoad = () => {
    controller.updateProgressBar(0);
    scene.load();
  };

  app.audioManager.load(song, fftSize);
};
