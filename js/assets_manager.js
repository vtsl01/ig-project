class AssetsManager {
  constructor () {
    this.manager = new THREE.LoadingManager();
    this.modelLoader = new THREE.GLTFLoader(this.manager);
    this.textureLoader = new THREE.TextureLoader(this.manager);
    this.textureLoader.setPath('assets/images/');
    this.cubemapLoader = new THREE.CubeTextureLoader(this.manager);
    this.cubemapLoader.setPath('assets/images/cubemaps/');

    this.assets = {
      models: {},
      textures: {},
      cubemaps: {}
    };

    this.manager.onStart = (url, itemsLoaded, itemsTotal) => {
      console.log('Started loading assets');
    };

    this.manager.onProgress = (url, itemsLoaded, itemsTotal) => {
      console.log('Loading file: ' + url);
      console.log('Loaded ' + itemsLoaded + ' of ' + itemsTotal + ' files');

      this.onProgress(itemsLoaded / itemsTotal);
    };

    this.manager.onLoad = () => {
      console.log('Loaded all assets');

      this.onLoad(this.assets);
    }

    this.manager.onError = (url) => {
      console.log('There was an error loading ' + url);
    };
  }

  loadModels (...descs) {
    const models = this.assets.models;

    for (let desc of descs) {
      this.modelLoader.load('assets/models/' + desc.url,
        function(gltf) {
          const model = gltf.scene;

          model.name = desc.name;
          model.animations = gltf.animations;

          models[desc.name] = model;

          console.log(desc.name + ' model loaded');
        },

        // onProgress (not used)
        undefined,

        function (error) {
          console.error(error);
        }
      );
    }
  }

  loadTextures (...descs) {
    const textures = this.assets.textures;

    for (let desc of descs) {
      this.textureLoader.load(desc.url,
        function(texture) {
          textures[desc.name] = texture;

          console.log(desc.name + ' texture loaded');
        },

        // onProgress (unsupported)
        undefined,

        function (error) {
          console.error(error);
        }
      );
    }
  }

  loadCubeMap (urls, name) {
    const cubemaps = this.assets.cubemaps;

    this.cubemapLoader.load(urls,
      function(cubemap) {
        cubemaps[name] = cubemap;

        console.log(name + ' cubemap loaded');
      },

      // onProgress (not used)
      undefined,

      function (error) {
        console.error(error);
      }
    );
  }
}
