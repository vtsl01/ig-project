class Controller {
  constructor (app) {
    this.app = app;
  }

  initUI (options = {}) {
    const cameraControls = new THREE.OrbitControls(this.app.camera);
    cameraControls.enableKeys = false;
    cameraControls.addEventListener('change', () => {
      if (!this.app.playing) {
        this.app.render();
      }
    });

    document.addEventListener('keydown', (event) => {
      this.app.game.inputManager.onKeyboardInput(event.keyCode, true);
    });

    document.addEventListener('keyup', (event) => {
      this.app.game.inputManager.onKeyboardInput(event.keyCode, false);
    });

    window.onresize = () => {
      this.app.resize();

      if (!this.app.playing) {
        this.app.render();
      }
    }

    const playInput = document.getElementById('playInput');
    playInput.checked = options.playing || false;
    playInput.oninput = (event) => {
      event.target.checked ? this.app.start() : this.app.stop();
    };

    this.progressWidget = {
      container: document.getElementById('progressBarContainer'),
      bar: document.getElementById('progressBar')
    };
  }

  updateProgressBar (progress) {
    const bar = this.progressWidget.bar;
    const max = bar.getAttribute('aria-valuemax');
    const min = bar.getAttribute('aria-valuemin');
    const value = progress * (max - min);

    bar.setAttribute('aria-valuenow', value);
    bar.style.width = value + '%';
  }

  showProgressBar () {
    this.progressWidget.container.style.visibility = 'visible';
  }

  hideProgressBar () {
    this.progressWidget.container.style.visibility = 'hidden';
  }
}
