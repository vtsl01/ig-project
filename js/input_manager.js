class InputManager {
  constructor () {
    this.keys = {
      LEFT: 37,
      UP: 38,
      RIGHT: 39,
      DOWN: 40
    };

    this.reset();
  }

  reset () {
    this.state = {
      up: { pos: 0, neg: 0 },
      rot: { pos: 0, neg: 0 }
    };
  }

  keyToMotion (key) {
    switch (key) {
      case this.keys.UP:
        return { axis: 'up', direction: 'pos' };
      case this.keys.DOWN:
        return { axis: 'up', direction: 'neg' };
      case this.keys.LEFT:
        return { axis: 'rot', direction: 'neg' };
      case this.keys.RIGHT:
        return { axis: 'rot', direction: 'pos' };
      default:
        return {}
    }
  }

  onKeyboardInput (key, pressed) {
    const { axis, direction } = this.keyToMotion(key);

    if (axis && direction) {
      this.state[axis][direction] = pressed ? 1 : 0;
    }
  }
}
