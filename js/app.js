class App {
  constructor (renderer, scene, camera, game) {
    this.renderer = renderer;
    this.scene = scene;
    this.camera = camera;
    this.game = game;
    this.playing = false;
    this.clock = new THREE.Clock(false);
    this.audioManager = new AudioManager();

    this.resize();
  }

  start() {
    this.playing = true;

    this.audioManager.sound.play();

    this.clock.start();

    this.renderer.setAnimationLoop(this.animate.bind(this));
  }

  stop() {
    this.renderer.setAnimationLoop(null);

    this.audioManager.sound.stop();

    this.clock.stop();

    this.playing = false;
  }

  animate () {
    if (this.audioManager.sound.isPlaying) {
      const audioSample = this.audioManager.analyze();

      this.animator.update(audioSample, this.clock.getDelta());

      this.render();
    } else {
      this.stop();
    }
  }

  render () {
    this.renderer.render(this.scene.root, this.camera);
  }

  // https://threejsfundamentals.org/threejs/lessons/threejs-responsive.html
  // Handle the pixel ratio manually because the real size could be needed
  // elsewhere. However it is not recommended for heavy apps.
  resize () {
    const canvas = this.renderer.domElement;
    const pixelRatio = window.devicePixelRatio || 1;
    const width = canvas.clientWidth * pixelRatio;
    const height = canvas.clientHeight * pixelRatio;

    this.renderer.setSize(width, height, false);
    this.camera.aspect = width / height;
    this.camera.updateProjectionMatrix();
  }
}
