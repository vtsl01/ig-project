'use-strict';

const Utils = {
  // https://threejsfundamentals.org/threejs/lessons/threejs-load-gltf.html
  dumpObject: function(obj, lines = [], isLast = true, prefix = '') {
    const localPrefix = isLast ? '└─' : '├─';

    lines.push(`${prefix}${prefix ? localPrefix : ''}${obj.name || '*no-name*'} [${obj.type}]`);

    const newPrefix = prefix + (isLast ? '  ' : '│ ');
    const lastIndex = obj.children.length - 1;

    obj.children.forEach((child, index) => {
      const isLast = index === lastIndex;
      Utils.dumpObject(child, lines, isLast, newPrefix);
    });

    return lines;
  }
}
