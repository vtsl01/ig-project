class PathSegment {
  constructor (points) {
    this.points = points;

    this.updateCurve();
  }

  origin () {
    return this.points[0];
  }

  end () {
    return this.points[this.points.length - 1];
  }

  empty () {
    return this.points.length == 0;
  }

  addPoints (points) {
    this.points = this.points.concat(points);
  }

  shiftPoints (count) {
    this.points.splice(0, count);
  }

  updateCurve () {
    if (this.empty()) {
      this.curve = undefined;
      this.length = 0;
    } else {
      this.curve = new THREE.CatmullRomCurve3(this.points);
      this.length = this.curve.getLength();
    }
  }

  getMesh (material, geometryOptions) {
    if (this.empty()) {
      return undefined;
    }

    const geometry = this.getGeometry(geometryOptions);

    return new THREE.Mesh(geometry, material);
  }

  getGeometry (options) {
    if (this.empty()) {
      return undefined;
    }

    const { radius, t_segs, r_segs } = options;

    return new THREE.TubeBufferGeometry(this.curve, t_segs, radius, r_segs);
  }
}
