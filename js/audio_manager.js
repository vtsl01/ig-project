class AudioManager {
  constructor() {
    this.listener = new THREE.AudioListener();
    this.loader = new THREE.AudioLoader();
    this.loader.setPath('assets/songs/');
    this.sound = new THREE.Audio(this.listener);
  }

  load (name, fftSize) {
    this.loader.load(name + '.mp3',
      (buffer) => {
        this.sound.setBuffer(buffer);
        this.analyser = new THREE.AudioAnalyser(this.sound, fftSize).analyser;

        console.log(name + ' song loaded');

        this.onLoad();
      },

      (progress) => {
        console.log('Loading song: ' + progress + '%');

        this.onProgress(progress);
      },

      (error) => {
        console.log(error);
      }
    );
  }

  analyze (callback) {
    const bufferLength = this.analyser.frequencyBinCount;
    const frequency = new Uint8Array(bufferLength);
    const time = new Uint8Array(bufferLength);

    this.analyser.getByteFrequencyData(frequency);
    this.analyser.getByteTimeDomainData(time);

    const sample = {
      frequency: frequency,
      time: time,
      n_bins: bufferLength,
    };

    return sample;
  }
}
