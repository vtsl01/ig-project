class Animator {
  constructor (scene, game, options = {}) {
    this.scene = scene;
    this.game = game;
    this.timeScale = options.timeScale || 1;

    this.pathRoot = this.scene.root.getObjectByName('pathRoot');
    this.path = this.pathRoot.userData.path;
    this.robotRoot = this.scene.root.getObjectByName('robotRoot');
    this.robot = this.robotRoot.getObjectByName('robot');
  }

  update (audiosample, deltaT) {
    deltaT *= this.timeScale;

    this.animatePath(deltaT);
    this.animateTargets(audiosample, deltaT);
    this.animateRobot(deltaT);
  }

  animatePath (deltaT) {
    const motion = this.game.state.path.motion;
    let currentSegment = this.path.origin();
    let l = currentSegment.length;
    let d = motion.speed * deltaT;
    let p = motion.p + (d / l);

    if (p >= 1) {
      // add a new segment
      const n = this.scene.options.path.num_knots - 4;
      const range = this.scene.options.path.range;
      const newKnots = [];
      for (let i = 0; i < n; i++) {
        let x = range.x * (0.5 - Math.random());
        let y = range.y * (0.5 - Math.random());

        newKnots.push(new THREE.Vector2(x, y));
      }
      this.path.addSegment(newKnots);

      // remove the first segment
      this.path.shiftSegment();

      // update the currentSegment
      currentSegment = this.path.origin();

      // update the current distance and arc length
      d = (p - 1) * l;
      p = d / currentSegment.length;

      // remove all the meshes
      this.pathRoot.children.forEach((mesh) => {
        mesh.geometry.dispose();
      });
      this.pathRoot.remove(...this.pathRoot.children);

      // add all the new meshes
      this.pathRoot.add(...this.path.getAlignedMeshes());

      // add the targets
      this.scene.addTargets();

      // reset the root position
      this.pathRoot.position.set(0, 0, 0);
    }

    // get the tangent at the current arc length
    const tangent = currentSegment.curve.getTangentAt(p);
    // move the path in the opposite direction of the tangent
    this.pathRoot.position.addScaledVector(tangent, -d);

    // update the current arc length
    motion.p = p;
  }

  animateTargets (audiosample, deltaT) {
    const segmentMesh = this.pathRoot.children[0];
    const targetsRoot = segmentMesh.getObjectByName('targetsRoot');
    const targets = targetsRoot.children;
    const motion = this.game.state.targets.motion;
    const { frequency, time, n_bins } = audiosample;
    const avgF = Math.round(frequency.reduce((a, b) => a + b, 0) / n_bins);
    const avgT = Math.round(time.reduce((a, b) =>  a + b, 0) / n_bins);

    const { x, y, z } = motion.limit;
    const n = targets.length;
    for (let i = 0; i < n; i++) {
      let target = targets[i];
      let f = frequency[i];
      let t = time[i];
      let initPosition = target.userData.initPosition;


      let dx = x * (t - avgT + 1) / 256;
      let dy = y * Math.sin(Math.PI * (0.5 - Math.random()));
      let dz = 0;

      let px = initPosition.x + dx;
      let py = initPosition.y + dy;
      let pz = initPosition.z + dz;

      target.position.set(px, py, pz);

      let r = 0.3 * (f + 1) / 256;
      let g = 0.6 * (f + 1) / 256;
      let b = 0.9 * (f + 1) / 256;
      target.material.color.setRGB(r, g, b);
    }
  }

  animateRobot (deltaT) {
    const root = this.robotRoot;
    const bot = this.robot;

    const { up, rot, speed, limit } = this.game.state.robot.motion;
    const input = this.game.inputManager.state;
    const delta = {
      up: input.up.pos - input.up.neg,
      rot: input.rot.pos - input.rot.neg
    };

    const p = this.game.state.path.motion.p;
    const tangent = this.path.origin().curve.getTangentAt(p);
    root.lookAt(tangent);

    const quaternion = new THREE.Quaternion();
    const newRot = rot + THREE.Math.degToRad(delta.rot * speed.rot);
    if (Math.abs(newRot) <= THREE.Math.degToRad(limit.rot)) {
      this.game.state.robot.motion.rot = newRot;
      quaternion.setFromAxisAngle(tangent, newRot);
    } else {
      quaternion.setFromAxisAngle(tangent, rot);
    }
    root.applyQuaternion(quaternion);

    const newUp = up + (delta.up * speed.up);
    if (newUp >= 0 && newUp <= limit.up) {
      this.game.state.robot.motion.up = newUp;
      bot.position.y = newUp + root.userData.radius;
    }

    bot.getObjectByName('Collo_02').rotation.y += 0.1
  }
}
